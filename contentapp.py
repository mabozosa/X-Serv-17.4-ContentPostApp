#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp

form = """
    <br/><br/>
    <form action="" method = "POST"> 
    <input type="text" name="name" value="">
    <input type="submit" value="Enviar">
    </form>
"""


class contentPostApp (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """Return the resource name (including /), method and body"""

        method, resource, _ = request.split(' ', 2)  # Trocea con espacio 2 veces y termina
        body = request.split('\r\n\r\n', 1)[1]  # Trocea /r/n/r/n 1 vez y termina (se queda con el cuerpo)
        print(request.split(' '))

        return method, resource, body

    def process(self, resourceName):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """

        method, resource, body = resourceName
        print('\r\nMétodo: ' + method, '\r\nRecurso: ' + resource, '\r\nBody: ' + body)

        if method == "POST":  # Método POST
            resource = '/' + body.split('=')[1]  # Se crea el recurso incluyendo la / (/recurso)
            self.content[resource] = body.split("=")[1]  # Guarda en el diccionario el contenido del
            # cuerpo (valor del formulario) asociado al nuevo recurso

        # Método GET contemplado aquí (en este caso si no es POST solo puede ser GET y no cambia el resto)
        if resource in self.content.keys():
            httpCode = "200 OK"
            # GET y POST muestran ambos el formulario y el contenido (en el caso de POST tras mandar el formulario)
            htmlBody = "<html><body>" + self.content[resource] + form + "</body></html>"
        else:  # Si no se encuentra el recurso se devuelve un error.
            httpCode = "404 Not Found"
            htmlBody = "<html><body>Not Found" + form + "</body></html>"
        return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = contentPostApp("localhost", 1234)
